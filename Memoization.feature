# Copyright 2018-2018 DevCraft, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Feature: Memoization

Scenario: First attempt at rendering in pass uses layer and is memoized
    Given layer L1 renders color C for pixel P
    And layer L1 is not memoized for pixel P
    When pixel P is rendered using layer L1
    Then pixel P = C
    And layer L1 is memoized for pixel P with color C

Scenario: Second attempt at rendering pass uses
    Given layer L1 is memozied for pixel P with color C
    When pixel P is rendered using layer L1
    Then pixel P = C
    And layer L1 was not rendered
    And layer L1 is memoized for pixel P with color C

Scenario: New pass clears memoization
    Given layer L1 has been memoized for pixel P with color C
    When a new rendering pass starts
    Then layer L1 is not memoized for pixel P with color C