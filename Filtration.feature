# Copyright 2018-2018 DevCraft, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Feature: Filters

Scenario Outline: Greenscreen
    Given layer L1 has a Greenscreen filter with a background color of (<ExcludeRGB>) and a tolerance of <ExcludeTolerance>.
    And layer L1 renders color (<LayerRGBA>) for pixel P
    When pixel P is rendered using layer L1
    Then pixel P has the value (<ExpectedRGBA>)
Scenarios:
    | ExcludeRGB    | ExcludeTolerance | LayerRGBA                 | ExpectedRGBA       |
    | 0%, 100%, 0%  | 0.1%             | 0%, 99.9%, 0%, 50%         | 0%, 99.9%, 0%, 50% |
    | 0%, 100%, 0%  | 0.1%             | 0.09%, 99.91%, 0.09%, 50%  | 0%, 0%, 0%, 0%     |

Scenario Outline: Ghost filter
    Given layer L1 has Ghost filter with alpha <GhostAlpha>
    And layer L1 renders color (50%, 75%, 50%, <LayerAlpha>) for pixel P
    When pixel P is rendered using layer L1
    Then pixel P has the value (50%, 75%, 50%, <ExpectedAlpha>)
Scenarios:
    | GhostAlpha    | LayerAlpha    | ExpectedAlpha |
    | 90%           | 100%          | 90%           |
    | 50%           | 50%           | 25%           |

Scenario Outline: Alpha floor
    Given layer L1 has Layer Floor filter with alpha <FloorAlpha>
    And layer L1 renders color (25%, 50%, 25%, <LayerAlpha>)
    When pixel P is rendered using layer L1
    Then pixel P has the value (25%, 50%, 25%, <ExpectedAlpha>)
Scenarios:
    | FloorAlpha    | LayerAlpha    | ExpectedAlpha |
    | 10%           | 11%           | 11%           |
    | 10%           | 10%           | 10%           |
    | 10%           | 9%            | 10%           |

Scenario Outline: Alpha ceiling
    Given layer L1 has Layer Floor filter with alpha <CeilingAlpha>
    And layer L1 renders color (25%, 50%, 25%, <LayerAlpha>)
    When pixel P is rendered using layer L1
    Then pixel P has the value (25%, 50%, 25%, <ExpectedAlpha>)
Scenarios:
    | CeilingAlpha  | LayerAlpha    | ExpectedAlpha |
    | 85%           | 86%           | 85%           |
    | 85%           | 85%           | 85%           |
    | 85%           | 84%           | 84%           |

Scenario Outline: Deaden
    Given layer L1 has Subtract filter with RGB (<SubtractRGB>)
    And layer L1 renders RGB (<LayerRGB>)
    When pixel P is rendered using layer L1
    Then pixel P has the RGB (<ExpectedRGB>)
Scenarios:
    | SubtractRGB   | LayerRGB          | ExpectedRGB       |
    | 0%, 0%, 5%    | 100%, 100%, 100%  | 100%, 100%, 95%   |
    | 0%, 0%, 5%    | 100%, 100%, 4%    | 100%, 100%, 0%    |
    | 0%, 50%, 0%   | 50%, 50%, 50%     | 50%, 0%, 50%      |
