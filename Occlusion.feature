# Copyright 2018-2018 DevCraft, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Feature: Layering

Scenario: Total Occlusion
    Given the following layers and outputs:
        | Layer | Pixel | Output RBG | Output Opacity |
        | L1    | P     | C          | 100%           |
        | L2    | P     | D          | 100%           |
    When pixel P is rendered with layers L1, L2
    Then pixel P = C

Scenario: Alpha blending
    Given the following layers and outputs:
        | Layer | Pixel | Output RBG | Output Opacity |
        | L1    | P     | C          | 75%            |
        | L2    | P     | D          | 100%           |
    When pixel P is rendered with layers L1, L2
    Then pixel P = C * .75 + D * .25

Scenario: Next layer
    Given the following layers and outputs:
        | Layer | Pixel | Output RBG | Output Opacity |
        | L1    | P     | C          | 0%             |
        | L2    | P     | D          | 100%           |
    When pixel P is rendered with layers L1, L2
    Then pixel P = D
